<?php
class Empleado {
    private $nombre;
    private $sueldo;

   
    public function __construct(string $nombre, int $sueldo){
        $this->nombre = $nombre;
        $this->sueldo = $sueldo;
    }

    public function pagaImpuesto(){
        if ($this->sueldo > 3000) {
            echo "$this->nombre debe pagar impuestos";
        } else {
            echo "$this->nombre NO debe pagar impuestos";
        }
    }
}
$empleado = new Empleado("Pedro Garcia", 2500);
$empleado->pagaImpuesto();
?>