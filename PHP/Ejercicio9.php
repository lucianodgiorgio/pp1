<?php
class Persona {
    private $nombre;
    private $edad;

    public function __construct(string $nombre, int $edad){
        $this->nombre = $nombre;
        $this->edad = $edad;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
    }

    public function setEdad($edad){
        $this->edad = $edad;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function getEdad(){
        return $this->edad;
    }
}

class Empleado extends Persona {
    private $sueldo;

    public function __construct(string $nombre, int $edad, int $sueldo){
        parent::__construct($nombre, $edad);
        $this->sueldo = $sueldo;
    }
    
    public function setNombre($nombre) {
        parent::setNombre();
    }

    public function getNombre() {
        return parent::getNombre();
    }

    public function setEdad($edad) {
        parent::setEdad();
    }

    public function getEdad() {
        return parent::getEdad();
    }
    
    public function setSueldo($sueldo){
        $this->sueldo = $sueldo;
    }

    public function getSueldo(){
        return $this->sueldo;
    }
}

$persona = new Persona ("Pedro Garcia", 35);
$empleado = new Empleado ("Juan Fernandez", 27, 50000);
echo "Primer Persona, no empleada\n";
$nombre1 = $persona->getNombre();
echo "Nombre: ".$nombre1;
$edad1 = $persona->getEdad();
echo "\nEdad: ".$edad1;
echo "\nSegunda Persona, empleada \n";
$nombre2 = $empleado->getNombre();
echo "Nombre: ".$nombre2;
$edad2 = $empleado->getEdad();
echo "\nEdad: ".$edad2;
$sueldo = $empleado->getSueldo();
echo "\nSueldo: ".$sueldo;
?>